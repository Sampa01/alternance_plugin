<?php
/*
Plugin Name: Candidate Registration
Description: A plugin to register candidates.
Version: 1.0
Author: Sampa
*/

// Enqueue Bootstrap Styles and Scripts
function enqueue_bootstrap_styles_scripts() {
    // Enqueue Bootstrap CSS
    wp_enqueue_style('bootstrap-css', 'https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css');

    // Enqueue Bootstrap JS and Popper.js (required for Bootstrap's JavaScript)
    wp_enqueue_script('lottie-web', 'https://cdnjs.cloudflare.com/ajax/libs/lottie-web/5.7.8/lottie.min.js', [], false, true);

    wp_enqueue_script('popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.3/umd/popper.min.js', [], false, true);
    wp_enqueue_script('bootstrap-js', 'https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js', ['jquery', 'popper'], false, true);
}
add_action('wp_enqueue_scripts', 'enqueue_bootstrap_styles_scripts');

// Check if form is submitted
function process_candidate_form() {
    if(
        isset($_POST['submit_candidate_form']) 
        && isset($_POST['_wpnonce']) 
        && wp_verify_nonce($_POST['_wpnonce'], 'candidate_registration_nonce') 
    ) {
        // Validation
        $required_fields = ['first_name', 'last_name', 'birth_date', 'diploma_title', 'study_level', 'desired_training', 'residence_city', 'mobility_km'];
        foreach($required_fields as $field) {
            if(empty($_POST[$field])) {
                error_log("Validation failed. $field is empty.");
                return; // Stop processing the form
            }
        }

        error_log("Form is being processed.");

        global $wpdb;
        $table_name = $wpdb->prefix . "candidate_registration";

        $data = array(
            'first_name' => sanitize_text_field($_POST['first_name']),
            'last_name' => sanitize_text_field($_POST['last_name']),
            'birth_date' => sanitize_text_field($_POST['birth_date']),
            'diploma_title' => sanitize_text_field($_POST['diploma_title']),
            'study_level' => sanitize_text_field($_POST['study_level']),
            'desired_training' => sanitize_text_field($_POST['desired_training']),
            'residence_city' => sanitize_text_field($_POST['residence_city']),
            'mobility_km' => intval($_POST['mobility_km']),
            'free_text' => sanitize_textarea_field($_POST['free_text'])
        );

        $success = $wpdb->insert($table_name, $data);

        if($success) {
            error_log("Data inserted successfully. Redirecting...");
            wp_redirect(add_query_arg('registration', 'success', get_permalink()));
            exit;
        } else {
            error_log("Error inserting data: " . $wpdb->last_error);
        }
    }
}
add_action('init', 'process_candidate_form');

// Function to create the database table
function create_candidate_table() {
    global $wpdb;
    $table_name = $wpdb->prefix . "candidate_registration";

    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        first_name varchar(255) NOT NULL,
        last_name varchar(255) NOT NULL,
        birth_date date NOT NULL,
        diploma_title varchar(255) NOT NULL,
        study_level varchar(255) NOT NULL,
        desired_training varchar(255) NOT NULL,
        residence_city varchar(255) NOT NULL,
        mobility_km int NOT NULL,
        free_text text,
        PRIMARY KEY  (id)
    ) $charset_collate;";

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql);
}
register_activation_hook(__FILE__, 'create_candidate_table');

// Display Candidate Form with Bootstrap styles
function display_candidate_form() {
    ob_start();

    if (isset($_GET['registration']) && $_GET['registration'] === 'success') {
        echo '<div class="alert alert-success">Registration successful!</div>';

        // Display Lottie animation container only after successful registration
        echo '<div id="lottie-animation" style="width: 200px; height: 200px;"></div>';

        echo '
        <script>
            document.addEventListener("DOMContentLoaded", function() {
                if (window.lottie) {
                    var animation = lottie.loadAnimation({
                        container: document.getElementById("lottie-animation"),
                        renderer: "svg",
                        loop: true,
                        autoplay: true,
                        path: "https://lottie.host/3f4e47ce-2ebc-4b96-9c08-990b9f92839e/4iAakXw6CU.json"
                    });
                } else {
                    console.error("Lottie library not loaded!");
                }
            });
        </script>';
    }

    ?>
    <form method="post" class="mt-3" onsubmit="return validateForm();">
        <div class="form-group">
            <label for="first_name">First Name:</label>
            <input type="text" class="form-control" id="first_name" name="first_name">
        </div>
        <div class="form-group">
            <label for="last_name">Last Name:</label>
            <input type="text" class="form-control" id="last_name" name="last_name">
        </div>
        <div class="form-group">
            <label for="birth_date">Date of Birth:</label>
            <input type="date" class="form-control" id="birth_date" name="birth_date">
        </div>
        <div class="form-group">
            <label for="diploma_title">Diploma Title:</label>
            <input type="text" class="form-control" id="diploma_title" name="diploma_title">
        </div>
        <div class="form-group">
            <label for="study_level">Study Level:</label>
            <input type="text" class="form-control" id="study_level" name="study_level">
        </div>
        <div class="form-group">
            <label for="desired_training">Desired Training:</label>
            <input type="text" class="form-control" id="desired_training" name="desired_training">
        </div>
        <div class="form-group">
            <label for="residence_city">Residence City:</label>
            <input type="text" class="form-control" id="residence_city" name="residence_city">
        </div>
        <div class="form-group">
            <label for="mobility_km">Mobility (in km):</label>
            <input type="number" class="form-control" id="mobility_km" name="mobility_km">
        </div>
        <div class="form-group">
            <label for="free_text">Free Text:</label>
            <textarea class="form-control" id="free_text" name="free_text"></textarea>
        </div>
        <?php wp_nonce_field('candidate_registration_nonce'); ?>
        <input type="submit" class="btn btn-primary" name="submit_candidate_form" value="Submit">
    </form>

    <script>
        function validateForm() {
            var firstName = document.getElementById("first_name").value;
            var lastName = document.getElementById("last_name").value;
            var birthDate = document.getElementById("birth_date").value;
            var diplomaTitle = document.getElementById("diploma_title").value;
            var studyLevel = document.getElementById("study_level").value;
            var desiredTraining = document.getElementById("desired_training").value;
            var residenceCity = document.getElementById("residence_city").value;
            var mobilityKm = document.getElementById("mobility_km").value;

            // Check if any of the required fields is empty
            if (
                firstName === "" ||
                lastName === "" ||
                birthDate === "" ||
                diplomaTitle === "" ||
                studyLevel === "" ||
                desiredTraining === "" ||
                residenceCity === "" ||
                mobilityKm === ""
            ) {
                alert("All fields are required. Please fill in all the required fields.");
                return false; // Prevent form submission
            }

            return true; // Allow form submission if all fields are filled
        }
    </script>
    <?php

    return ob_get_clean();
}
add_shortcode('candidate_form', 'display_candidate_form');


// Function to display the content of the candidate registration table
function display_candidate_table_content() {
    global $wpdb;
    $table_name = $wpdb->prefix . "candidate_registration";

    $results = $wpdb->get_results("SELECT * FROM $table_name", ARRAY_A);

    if (empty($results)) {
        echo '<p>No data available.</p>';
        return;
    }

    echo '<table class="table">';
    echo '<thead>';
    echo '<tr>';
    echo '<th>ID</th>';
    echo '<th>First Name</th>';
    echo '<th>Last Name</th>';
    echo '<th>Date of Birth</th>';
    echo '<th>Diploma Title</th>';
    echo '<th>Study Level</th>';
    echo '<th>Desired Training</th>';
    echo '<th>Residence City</th>';
    echo '<th>Mobility (in km)</th>';
    echo '<th>Free Text</th>';
    echo '</tr>';
    echo '</thead>';
    echo '<tbody>';

    foreach ($results as $row) {
        echo '<tr>';
        echo '<td>' . $row['id'] . '</td>';
        echo '<td>' . $row['first_name'] . '</td>';
        echo '<td>' . $row['last_name'] . '</td>';
        echo '<td>' . $row['birth_date'] . '</td>';
        echo '<td>' . $row['diploma_title'] . '</td>';
        echo '<td>' . $row['study_level'] . '</td>';
        echo '<td>' . $row['desired_training'] . '</td>';
        echo '<td>' . $row['residence_city'] . '</td>';
        echo '<td>' . $row['mobility_km'] . '</td>';
        echo '<td>' . $row['free_text'] . '</td>';
        echo '</tr>';
    }

    echo '</tbody>';
    echo '</table>';
}

// Shortcode to display the candidate table content
function display_candidate_table_shortcode() {
    ob_start();
    display_candidate_table_content();
    return ob_get_clean();
}
add_shortcode('candidate_table', 'display_candidate_table_shortcode');


?>